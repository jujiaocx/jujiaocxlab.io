export const blogs = [
  {
    Title: "关于我们",
    Uid: "about",
    FileLink: "https://cdn.jsdelivr.net/gh/jujiaocx/besthub/docs/about.md",
    BaseUrl: "/blog/about",
    CreateAt: "2020-09-08",
  },
  {
    Title: "联系我们",
    Uid: "contact",
    FileLink: "https://cdn.jsdelivr.net/gh/jujiaocx/besthub/docs/contact.md",
    BaseUrl: "/blog/contact",
    CreateAt: "2020-09-12",
  },
];

export const typeList = [
  {
    Id: "up",
    Type: "up",
    Title: "Up主",
    BaseUrl: "/v/up",
  },
  {
    Id: "musician",
    Type: "musician",
    Title: "音乐家",
    BaseUrl: "/v/musician",
  },
  {
    Id: "enterprise",
    Type: "enterprise",
    Title: "企业家",
    BaseUrl: "/v/enterprise",
  },
  {
    Id: "president",
    Type: "president",
    Title: "总统",
    BaseUrl: "/v/president",
  },
];

export const personList = [
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/1.jpg",
    Title: "真不倒翁",
    Email: "jujiaocx@gmail.com",
    Location: "中国 深圳",
    Profile: "创作来源于生活",
    Tags: [
      {
        Name: "自媒体",
      },
      {
        Name: "互联网",
      },
      {
        Name: "技术",
      },
      {
        Name: "程序员",
      },
      {
        Name: "博客",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/7510242325",
        },
        // {
        //   Title: "weixin",
        //   Link: "",
        // },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/109923242878",
        },
        // {
        //   Title: "xiaohongshu",
        //   Link: "",
        // },
        {
          Title: "zhihu",
          Link: "https://www.zhihu.com/people/jujiaocx",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/94630686",
        },
      ],
      Out: [
        {
          Title: "twitter",
          Link: "https://twitter.com/jujiaocx",
        },
        // {
        //   Title: "youtube",
        //   Link: "",
        // },
        // {
        //   Title: "instagram",
        //   Link: "",
        // },
        // {
        //   Title: "facebook",
        //   Link: "",
        // },
        // {
        //   Title: "reddit",
        //   Link: "",
        // },
        {
          Title: "github",
          Link: "https://github.com/jujiaocx",
        },
        {
          Title: "telegram",
          Link: "https://telegram.me/jujiaocx",
        },
        // {
        //   Title: "linkedin",
        //   Link: "",
        // },
      ],
    },
    Content: [
      {
        Title: "苹果手机发展史，也许你正在用这些？",
        Link: "https://www.toutiao.com/item/6883473129901916675/",
        ImageUrl:
          "https://p9-tt-ipv6.byteimg.com/large/pgc-image/1741f29d24b74b49a5a179640636464e",
      },
      {
        Title: "至今无冠的NBA球队",
        Link: "https://www.toutiao.com/i6882743668860682759/",
        ImageUrl:
          "https://p26-tt.byteimg.com/large/pgc-image/dd412af3eca74b6fb134a0067b89ab18",
      },
      {
        Title: "历届NBA总决赛最有价值球员",
        Link: "https://www.toutiao.com/item/6884586435048014349/",
        ImageUrl:
          "https://p1-tt-ipv6.byteimg.com/large/pgc-image/3e352ba290984d75825a642b917262be",
      },
    ],
    Honor: [
      {
        Name: "BestHub 站点创始人",
      },
    ],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/budaoweng",
    Uid: "budaoweng",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/2.jpg",
    Title: "老番茄",
    Email: "",
    Location: "中国 上海",
    Profile: "游戏区最菜up主，还是个孩子你们快欺负他",
    Tags: [
      {
        Name: "我的MA呀",
      },
      {
        Name: "财大NB",
      },
      {
        Name: "死神，来收人了",
      },
      {
        Name: "视频创作",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/laofanqie",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/546195",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/61569537050/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCSN9fvRC2vzI1H6QCWZDZNg",
        },
      ],
    },
    Content: [],
    Honor: [
      { Name: "知名游戏博主" },
      { Name: "bilibili 2019百大UP主" },
      { Name: "bilibili 高能联盟成员" },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E8%80%81%E7%95%AA%E8%8C%84",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E8%80%81%E7%95%AA%E8%8C%84",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/laofanqie",
    Uid: "laofanqie",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/3.jpg",

    Title: "罗翔说刑法",
    Email: "",
    Location: "中国 北京",
    Profile: "厚大法考刑法独家老师。热点事件点评，读书体会，做法治之光。",
    Tags: [
      {
        Name: "中国政法大学教授",
      },
      {
        Name: "厚大法考刑法独家教师",
      },
      {
        Name: "刑法",
      },
      {
        Name: "张三",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/2610429597",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/517327498",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/712099224617048/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [
      {
        Name: "bilibili 高能联盟成员",
      },
      {
        Name: "代表作《刑法学讲义》《圆圈正义》《罗翔讲刑法》",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E7%BD%97%E7%BF%94_(%E6%95%99%E6%8E%88)",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E7%BD%97%E7%BF%94/24167665",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/luoxiang",
    Uid: "luoxiang",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/4.jpg",
    Title: "LexBurner",
    Email: "",
    Location: "中国 上海",
    Profile: "B站毒舌漫评up主，原创视频制作人。",
    Tags: [
      {
        Name: "动漫达人",
      },
      {
        Name: "动漫视频自媒体",
      },
      {
        Name: "游戏实况吐槽主播",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "http://weibo.com/lexburner",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/777536",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/50098741957/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UC0mKktFsdVADD4YJWDKuZVw",
        },
      ],
    },
    Content: [],
    Honor: [
      {
        Name: "bilibili 2019百大UP主",
      },
      {
        Name: "bilibili 高能联盟成员",
      },
      {
        Name: "代表作Lex吐槽系列",
      },
    ],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/LexBurner",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/lexburner",
    Uid: "lexburner",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/5.jpg",
    Title: "敬汉卿",
    Email: "1905387728@qq.com",
    Location: "中国 上海",
    Profile: "一个喜欢作死的天文学爱好者",
    Tags: [
      {
        Name: "美食",
      },
      {
        Name: "搞笑视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/hongrenlaohan",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/9824766",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5725027217/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCcwITi7pENz_dbzvmgMmESw",
        },
      ],
    },
    Content: [],
    Honor: [
      {
        Name: "bilibili 2019百大UP主",
      },
      {
        Name: "2019年度弹幕人气奖UP主",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E6%95%AC%E6%B1%89%E5%8D%BF",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E6%95%AC%E6%B1%89%E5%8D%BF",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/jinghanqing",
    Uid: "jinghanqing",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/6.jpg",
    Title: "凉风Kaze",
    Email: "",
    Location: "中国 上海",
    Profile: "动漫视频阿婆主，有脑洞，有吐槽。",
    Tags: [
      {
        Name: "不正经解说",
      },
      { Name: "阅片无数" },
      {
        Name: "知名动漫博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/pangcijun",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/14110780",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5782567442/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [
      {
        Name: "bilibili 2019百大UP主",
      },
      {
        Name: "bilibili 高能联盟成员",
      },
    ],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/pangcijun",
    Uid: "pangcijun",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/7.jpg",

    Title: "敖厂长",
    Email: "",
    Location: "四川 成都",
    Profile: "《囧的呼唤》系列作者，单机游戏恶搞讲解类视频作者",
    Tags: [
      {
        Name: "业余视频作者",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/hawkao",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/122879",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5503060881",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E6%95%96%E5%8E%82%E9%95%BF",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E6%95%96%E5%8E%82%E9%95%BF",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/aochangzhang",
    Uid: "aochangzhang",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/8.jpg",

    Title: "周六野Zoey",
    Email: "wildsaturday.zoey@gmail.com",
    Location: "海外 美国",
    Profile: "品牌创始人，自媒体，认证私人健身教练",
    Tags: [
      {
        Name: "品牌创始人",
      },
      {
        Name: "自媒体",
      },
      {
        Name: "认证私人健身教练",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/hellozoeyhello",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/62540916",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/108288397099/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E5%91%A8%E5%85%AD%E9%87%8EZoey",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/zoey",
    Uid: "zoey",
  },
  {
    LogoUrl: "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/9.jpg",

    Title: "李子柒",
    Email: "liziqistyle@163.com",
    Location: "中国 ",
    Profile: "李家有女，人称子柒。",
    Tags: [
      {
        Name: "品牌创始人",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/mianyangdanshen",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/19577966",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/52773485452/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E6%9D%8E%E5%AD%90%E6%9F%92",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E6%9D%8E%E5%AD%90%E6%9F%92/22373329",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/liziqi",
    Uid: "liziqi",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/10.jpg",

    Title: "papi酱",
    Email: "syhz@papitube.com",
    Location: "中国 ",
    Profile: "一个集美貌与才华于一身的女子。",
    Tags: [
      {
        Name: "搞笑视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/xiaopapi",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/1532165",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5726188736/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/Papi%E9%85%B1",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/papi%E9%85%B1/19324554",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/papi",
    Uid: "papi",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/11.jpg",

    Title: "中国BOY超级大猩猩",
    Email: "",
    Location: "中国 上海",
    Profile: "我是中国拜，越菜也爱玩！",
    Tags: [
      {
        Name: "自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "http://weibo.com/qqqwwe",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/562197",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/61312613337/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E7%8E%8B%E7%80%9A%E5%93%B2",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/chinaboy",
    Uid: "chinaboy",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/12.jpg",

    Title: "机智的党妹",
    Email: "rikawan@lucnova.cn",
    Location: "中国 ",
    Profile: "爱比讨厌更有力量。",
    Tags: [
      {
        Name: "化妆",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/dzwlhlhlh",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/466272",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/1939182024395235/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E6%9C%BA%E6%99%BA%E7%9A%84%E5%85%9A%E5%A6%B9",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/dangmei",
    Uid: "dangmei",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/13.jpg",

    Title: "某幻君",
    Email: "dreamsnow713@qq.com",
    Location: "中国 上海",
    Profile: "知名游戏解说 娱乐视频自媒体",
    Tags: [
      {
        Name: "知名游戏博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "http://weibo.com/mouhuanjun",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/1577804",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/78925278510/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E6%9F%90%E5%B9%BB%E5%90%9B",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/mouhuanjun",
    Uid: "mouhuanjun",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/14.jpg",

    Title: "华农兄弟",
    Email: "",
    Location: "中国 江西",
    Profile: "实拍农村美食美景，有趣好玩新鲜事！",
    Tags: [
      {
        Name: "视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/3316146805",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/250858633",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/6806857445/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E8%8F%AF%E8%BE%B2%E5%85%84%E5%BC%9F",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E5%8D%8E%E5%86%9C%E5%85%84%E5%BC%9F",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/huanongxiongdi",
    Uid: "huanongxiongdi",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/15.jpg",

    Title: "我是郭杰瑞",
    Email: "",
    Location: "海外 美国",
    Profile: "Fibo咖啡创始人",
    Tags: [
      {
        Name: "资讯视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/5626136031",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/176037767",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/60140020874/",
        },
        {
          Title: "zhihu",
          Link: "https://www.zhihu.com/people/guojierui123",
        },
        {
          Title: "xiaohongshu",
          Link:
            "https://www.xiaohongshu.com/user/profile/5cb97a38000000001202c76c",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCfIbForcbE83cxm8MScOTlQ",
        },
        {
          Title: "facebook",
          Link: "https://www.facebook.com/IamGuoJieRui/",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E9%83%AD%E6%9D%B0%E7%91%9E",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E9%83%AD%E6%9D%B0%E7%91%9E",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/guojierui",
    Uid: "guojierui",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/16.jpg",

    Title: "指法芬芳张大仙",
    Email: "",
    Location: "中国 上海",
    Profile: "指法芬芳张大仙z",
    Tags: [
      {
        Name: "知名游戏博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/1811893237",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/1935882",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/55453255774/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E5%BC%A0%E5%A4%A7%E4%BB%99",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E5%BC%A0%E5%AE%8F%E5%8F%91/22376031",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/zhangdaxian",
    Uid: "zhangdaxian",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/17.jpg",

    Title: "绵羊料理",
    Email: "",
    Location: "中国 湖南",
    Profile: "对着油炸的卡路里许愿，瘦到90斤！",
    Tags: [
      {
        Name: "美食视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/sheephoho",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/18202105",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/6280259261/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E7%BB%B5%E7%BE%8A%E6%96%99%E7%90%86",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/sheephoho",
    Uid: "sheephoho",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/18.jpg",

    Title: "老师好我叫何同学",
    Email: "xhaxx1123@163.com",
    Location: "中国 上海",
    Profile: "放假会做贼有意思的视频",
    Tags: [
      {
        Name: "知名数码博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/6529876887",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/163637592",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/103239087833/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E8%80%81%E5%B8%88%E5%A5%BD%E6%88%91%E5%8F%AB%E4%BD%95%E5%90%8C%E5%AD%A6",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/hetongxue",
    Uid: "hetongxue",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/19.jpg",
    Title: "徐大虾",
    Email: "",
    Location: "中国 重庆",
    Profile: "b站某不知名up主",
    Tags: [
      {
        Name: "",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/YKxudaxia",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/13354765",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E5%BE%90%E5%A4%A7%E8%99%BE",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/xudaxia",
    Uid: "xudaxia",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/20.jpg",

    Title: "渗透之C君",
    Email: "3516477560@qq.com",
    Location: "美国",
    Profile: "恐怖游戏UP主",
    Tags: [
      {
        Name: "恐怖游戏UP主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/roshinichi",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/4162287",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E6%B8%97%E9%80%8F%E4%B9%8BC%E5%90%9B",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/cantouzhicjun",
    Uid: "cantouzhicjun",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/21.jpg",

    Title: "朱一旦的枯燥生活",
    Email: "",
    Location: "中国 上海",
    Profile: "黑色幽默，感谢关注！",
    Tags: [
      {
        Name: "知名搞笑幽默博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/zhugen",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/437316738",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/106074759431/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E6%9C%B1%E4%B8%80%E6%97%A6%E7%9A%84%E6%9E%AF%E7%87%A5%E7%94%9F%E6%B4%BB",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E6%9C%B1%E4%B8%80%E6%97%A6",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/zhugen",
    Uid: "zhugen",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/22.jpg",

    Title: "木鱼水心",
    Email: "bd@ark-tv.com",
    Location: "中国 北京",
    Profile: "影评某种程度上，是让看过这部片的人，不那么孤独的东西。",
    Tags: [
      {
        Name: "电影视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/acgpub",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/927587",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5837392222/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/muyushuixin",
    Uid: "muyushuixin",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/23.jpg",

    Title: "信誓蛋蛋",
    Email: "hey@xinshidandan.fr",
    Location: "美国",
    Profile: "搞笑幽默",
    Tags: [
      {
        Name: "搞笑幽默",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/realxinshidandan",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/32786875",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/50090537720/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E4%BF%A1%E8%AA%93%E8%9B%8B%E8%9B%8B",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/Real%E4%BF%A1%E8%AA%93%E8%9B%8B%E8%9B%8B",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/xinshidandan",
    Uid: "xinshidandan",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/24.jpg",

    Title: "美食作家王刚",
    Email: "",
    Location: "中国 四川",
    Profile:
      "美食作家王刚（线上很强大的家常菜教学之一）提升国民对美食的认知、追求",
    Tags: [
      {
        Name: "美食视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/6288254740",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/290526283",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/3731405128/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/wiki/%E7%8E%8B%E5%89%9B_(%E5%BB%9A%E5%B8%AB)",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E7%8E%8B%E5%88%9A/23623830",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/wanggang",
    Uid: "wanggang",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/25.jpg",

    Title: "半佛仙人",
    Email: "",
    Location: "中国",
    Profile: "喜欢小仙女",
    Tags: [
      {
        Name: "原创视频博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/5212695504",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/37663924",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/3490578743/",
        },
        {
          Title: "zhihu",
          Link: "https://www.zhihu.com/people/banfoxianren",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/banfoxianren",
    Uid: "banfoxianren",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/26.jpg",

    Title: "记录生活的蛋黄派",
    Email: "",
    Location: "中国 广东",
    Profile: "知名搞笑幽默博主",
    Tags: [
      {
        Name: "知名搞笑幽默博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/6577232105",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/337521240",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/84807220809/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/danhuangpai",
    Uid: "danhuangpai",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/27.jpg",

    Title: "花少北",
    Email: "",
    Location: "中国 河北",
    Profile: "精神分裂症+间歇性失忆症",
    Tags: [
      {
        Name: "知名游戏博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/5220398012",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/2206456",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E8%8A%B1%E5%B0%91%E5%8C%97",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/huashaobei",
    Uid: "huashaobei",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/28.jpg",

    Title: "手工耿",
    Email: "",
    Location: "中国 河北",
    Profile: "喜欢做一些有趣的小手工。欢迎提供一些有意思想法。谢谢",
    Tags: [
      {
        Name: "搞笑视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/3108949955",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/280793434",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/97716042466/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-cn/%E8%80%BF%E5%B8%85",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E6%89%8B%E5%B7%A5%E8%80%BF",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/shougonggeng",
    Uid: "shougonggeng",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/29.jpg",

    Title: "小潮院长",
    Email: "2284771898@qq.com",
    Location: "中国 上海",
    Profile: "就是一个做视频的！",
    Tags: [
      {
        Name: "原创视频博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/xiaochao233",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/5970160",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/2510929813844061/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCbV9gFFSk2ypYO1CbEgLa8w",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E5%B0%8F%E6%BD%AE%E9%99%A2%E9%95%BF",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/xiaochao",
    Uid: "xiaochao",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/30.jpg",

    Title: "逍遥散人",
    Email: "xysrgames@126.com",
    Location: "中国 上海",
    Profile: "做视频的，希望大家天天开心！",
    Tags: [
      {
        Name: "游戏视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/xysanren",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/168598",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/99925671997/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCTfRwznpxtbjQQQJ_15Fk2w",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E9%80%8D%E9%81%A5%E6%95%A3%E4%BA%BA/3558908",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/xysanren",
    Uid: "xysanren",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/31.jpg",

    Title: "张召忠",
    Email: "",
    Location: "中国 北京",
    Profile: "各位网（玩）友（艺儿）大家好，我是局座张召忠。",
    Tags: [
      {
        Name: "军事专家",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/6049590367",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/33683045",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/6026436452/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-hans/%E5%BC%A0%E5%8F%AC%E5%BF%A0",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E5%BC%A0%E5%8F%AC%E5%BF%A0/29193",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/zhangzhaozhong",
    Uid: "zhangzhaozhong",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/33.jpg",

    Title: "努力的Lorre",
    Email: "",
    Location: "中国 北京",
    Profile: "讲述漫威DC的更多故事！",
    Tags: [
      {
        Name: "动漫视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/JimLorre",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/7487399",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/6868913773/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCKMZ34aa78_8tsPpIeuIMmA",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/JimLorre",
    Uid: "JimLorre",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/34.jpg",

    Title: "吃素的狮子",
    Email: "linjiaban2016@sina.cn",
    Location: "中国 上海",
    Profile: "网络生活、搞笑视频作者。",
    Tags: [
      {
        Name: "vlog博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/lion2233",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/808171",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/62226662072/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E5%90%83%E7%B4%A0%E7%9A%84%E7%8B%AE%E5%AD%90/19140470",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/lion2233",
    Uid: "lion2233",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/35.jpg",

    Title: "黑镖客梦回",
    Email: "",
    Location: "中国 四川",
    Profile: "游戏博主",
    Tags: [
      {
        Name: "游戏博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/heipiaoke",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/10558098",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/62126733585/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCmDguW2dEDyM5saVbkB91gg",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [],
    TypeId: "up",
    BaseUrl: "/p/heipiaoke",
    Uid: "heipiaoke",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/36.jpg",

    Title: "李永乐老师",
    Email: "",
    Location: "中国 北京",
    Profile: "中学教师，西瓜视频创作人，科普博主。",
    Tags: [
      {
        Name: "教育博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/3325704142",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/9458053",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/4234740937/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCSs4A6HYKmHA2MG_0z-F0xw",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-hans/%E6%9D%8E%E6%B0%B8%E4%B9%90",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E6%9D%8E%E6%B0%B8%E4%B9%90/1302180",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/liyongle",
    Uid: "liyongle",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/37.jpg",

    Title: "回形针PaperClip",
    Email: "paperclip@foxmail.com",
    Location: "中国 北京",
    Profile: "你的当代生活说明书，每周不定期更新。（未成年人请在家长陪同下观看",
    Tags: [
      {
        Name: "泛科普视频自媒体",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/6414205745",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/258150656",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/11568249695/",
        },
        {
          Title: "zhihu",
          Link: "https://zhuanlan.zhihu.com/c_145700634",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCUGJ-yKqQHl4FSZwUmGpiUg",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/zh-hans/%E5%9B%9E%E5%BD%A2%E9%92%88PaperClip",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E5%9B%9E%E5%BD%A2%E9%92%88",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/huixingzhen",
    Uid: "huixingzhen",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/38.jpg",

    Title: "罗永浩",
    Email: "",
    Location: "中国 北京",
    Profile: "Smartisan，智能机时代的工匠",
    Tags: [
      {
        Name: "锤子科技 CEO",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/laoluoyonghao",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/5469817756/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-hans/%E7%BD%97%E6%B0%B8%E6%B5%A9",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E7%BD%97%E6%B0%B8%E6%B5%A9/26814",
      },
    ],
    TypeId: "enterprise",
    BaseUrl: "/p/laoluoyonghao",
    Uid: "laoluoyonghao",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/39.jpg",

    Title: "雷军",
    Email: "",
    Location: "中国 北京",
    Profile: "小米董事长，金山软件董事长。业余爱好是天使投资。",
    Tags: [
      {
        Name: "小米创办人",
      },
      {
        Name: "董事长兼CEO",
      },
      {
        Name: "金山软件董事长",
      },
      {
        Name: "天使投资人",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/leijun",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/646730844",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/3334438776/",
        },
      ],
      Out: [],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-hans/%E9%9B%B7%E5%86%9B",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E9%9B%B7%E5%86%9B/1968",
      },
    ],
    TypeId: "enterprise",
    BaseUrl: "/p/leijun",
    Uid: "leijun",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/40.jpg",

    Title: "李自然说",
    Email: "",
    Location: "中国 上海",
    Profile: "有趣的灵魂 聊科技人文",
    Tags: [
      {
        Name: "微博原创视频博主",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "weibo",
          Link: "https://weibo.com/u/1027417317",
        },
        {
          Title: "bilibili",
          Link: "https://space.bilibili.com/39089748",
        },
        {
          Title: "toutiao",
          Link: "http://www.toutiao.com/c/user/63608992271/",
        },
      ],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCgLUl1WDoDXUtxPaZeSZHsw",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E6%9D%8E%E8%87%AA%E7%84%B6?force=1",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/liziranshuo",
    Uid: "liziranshuo",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/41.jpg",

    Title: "老高與小茉 Mr & Mrs Gao",
    Email: "gao@jp1.com",
    Location: "日本",
    Profile: "在這個頻道里我們會定期更新有趣的影片，希望大家能夠喜歡。",
    Tags: [
      {
        Name: "自媒体",
      },
    ],
    FocusUs: {
      In: [],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCMUnInmOkrWN4gof9KlhNmQ",
        },
        {
          Title: "facebook",
          Link:
            "https://www.facebook.com/%E8%80%81%E9%AB%98%E8%88%87%E5%B0%8F%E8%8C%89-Mr-Mrs-Gao-109557440516292/",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh-cn/%E8%80%81%E9%AB%98",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/laogaoyuxiaomo",
    Uid: "laogaoyuxiaomo",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/42.jpg",

    Title: "Marques Brownlee",
    Email: "",
    Location: "美国",
    Profile:
      "Quality Tech Videos | YouTuber | Geek | Consumer Electronics | Tech Head | Internet Personality!",
    Tags: [
      {
        Name: "Quality Tech Videos",
      },
    ],
    FocusUs: {
      In: [],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCBJycsmduvYEL83R_U4JriQ",
        },
        {
          Title: "twitter",
          Link: "https://twitter.com/mkbhd",
        },
        {
          Title: "instagram",
          Link: "https://www.instagram.com/mkbhd/",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://en.wikipedia.org/wiki/Marques_Brownlee",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/mkbhd",
    Uid: "mkbhd",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/43.jpg",

    Title: "阿滴英文",
    Email: "",
    Location: "中国 台湾",
    Profile:
      "哈咯我是阿滴，在这个频道上我会透过各种有趣的主題分享英文。希望能够让你重新点燃对英文学习的兴趣！偶尔我也会透过影片分享自己的生活。",
    Tags: [
      {
        Name: "英文",
      },
    ],
    FocusUs: {
      In: [],
      Out: [
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UCeo3JwE3HezUWFdVcehQk9Q",
        },
        {
          Title: "facebook",
          Link: "https://www.facebook.com/RayDuEnglish/",
        },
      ],
    },
    Content: [],
    Honor: [],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/zh-cn/%E9%98%BF%E6%BB%B4%E8%8B%B1%E6%96%87",
      },
    ],
    TypeId: "up",
    BaseUrl: "/p/adiyingwen",
    Uid: "adiyingwen",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/44.jpg",
    Title: "周杰伦",
    Email: "",
    Location: "中国 台湾",
    Profile: "哎哟，不错喔！",
    Tags: [
      {
        Name: "歌手",
      },
      {
        Name: "电影导演",
      },
      {
        Name: "音乐制作人",
      },
      {
        Name: "商人",
      },
    ],
    FocusUs: {
      In: [
        {
          Title: "douban",
          Link: "https://music.douban.com/musician/104916/",
        },
      ],
      Out: [
        {
          Title: "facebook",
          Link: "https://www.facebook.com/jay",
        },
        {
          Title: "instagram",
          Link: "https://www.instagram.com/jaychou/",
        },
        {
          Title: "youtube",
          Link: "https://www.youtube.com/channel/UC8CU5nVhCQIdAGrFFp4loOQ",
        },
        {
          Title: "imdb",
          Link: "https://www.imdb.com/name/nm01727100/",
        },
      ],
    },
    Content: [
      {
        Title: "2000年 Jay",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2001年 范特西",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2002年 八度空间",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2003年 叶惠美",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2004年 七里香",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2005年 11月的肖邦",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2006年 依然范特西",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2007年 我很忙",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2008年 魔杰座",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2010年 跨时代",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2011年 惊叹号",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2012年 十二新作",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2014年 哎呦，不错哦",
        Link: "",
        ImageUrl: "",
      },
      {
        Title: "2016年 周杰伦的床边故事",
        Link: "",
        ImageUrl: "",
      },
    ],
    Honor: [
      {
        Name:
          "2003年2月，《时代》杂志亚洲版封面华人歌手，被赞誉为「新一代亚洲流行天王」。",
      },
      {
        Name: "2009年入选世界十大杰出青年代表之一。",
      },
      {
        Name: "2011年4月21日，入选美国《时代》杂志评全球最具影响力人物100强",
      },
      {
        Name: "2012年5月，登上福布斯中国名人榜第一名",
      },
      {
        Name: "2019年7月21日，新浪微博超话影响力突破一亿，位居排行榜第一名。",
      },
      {
        Name: "入选2019年福布斯中国名人榜。",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/zh/%E5%91%A8%E6%9D%B0%E5%80%AB",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E5%91%A8%E6%9D%B0%E4%BC%A6",
      },
    ],
    TypeId: "musician",
    BaseUrl: "/p/jaychou",
    Uid: "jaychou",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/45.jpg",

    Title: "Joe Biden",
    Email: "",
    Location: "美国",
    Profile:
      "Senator, Vice President, 2020 candidate for President of the United States",
    Tags: [
      {
        Name: "美国副总统",
      },
      {
        Name: "总统竞选人",
      },
    ],
    FocusUs: {
      In: [],
      Out: [
        {
          Title: "twitter",
          Link: "https://twitter.com/JoeBiden",
        },
        {
          Title: "facebook",
          Link: "https://www.facebook.com/joebiden/",
        },
        {
          Title: "instagram",
          Link: "https://www.instagram.com/joebiden/",
        },
        {
          Title: "youtube",
          link: "https://www.youtube.com/c/JoeBiden",
        },
      ],
    },
    Content: [],
    Honor: [
      {
        Name: "美国副总统",
      },
      {
        Name: "2020美国总统竞选人",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/zh-cn/%E4%B9%94%C2%B7%E6%8B%9C%E7%99%BB",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E7%BA%A6%E7%91%9F%E5%A4%AB%C2%B7%E6%8B%9C%E7%99%BB",
      },
    ],
    TypeId: "president",
    BaseUrl: "/p/joebiden",
    Uid: "joebiden",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/46.jpg",

    Title: "Donald J.Trump",
    Email: "",
    Location: "美国",
    Profile: "45th President of the United States of America",
    Tags: [
      {
        Name: "美国总统",
      },
      {
        Name: "总统竞选人",
      },
      {
        Name: "川普",
      },
      {
        Name: "特朗普",
      },
    ],
    FocusUs: {
      In: [],
      Out: [
        {
          Title: "twitter",
          Link: "https://twitter.com/realDonaldTrump",
        },
        {
          Title: "facebook",
          Link: "https://www.facebook.com/DonaldTrump",
        },
        {
          Title: "instagram",
          Link: "https://www.instagram.com/realdonaldtrump/",
        },
        {
          Title: "youtube",
          link: "https://www.youtube.com/Donaldtrump",
        },
      ],
    },
    Content: [],
    Honor: [
      {
        Name: "美国第45届总统",
      },
      {
        Name: "2020美国总统竞选人",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link:
          "https://zh.wikipedia.org/zh-cn/%E5%94%90%E7%B4%8D%C2%B7%E5%B7%9D%E6%99%AE",
      },
      {
        Title: "百度百科",
        Link:
          "https://baike.baidu.com/item/%E5%94%90%E7%BA%B3%E5%BE%B7%C2%B7%E7%89%B9%E6%9C%97%E6%99%AE",
      },
    ],
    TypeId: "president",
    BaseUrl: "/p/donaldtrump",
    Uid: "donaldtrump",
  },
  {
    LogoUrl:
      "https://cdn.jsdelivr.net/gh/jujiaocx/BestHub/images/p-logo/47.jpg",
    Title: "马云",
    Email: "",
    Location: "中国 杭州",
    Profile: "",
    Tags: [
      {
        Name: "风清扬",
      },
      {
        Name: "阿里巴巴",
      },
      {
        Name: "教师",
      },
      {
        Name: "企业家",
      },
    ],
    FocusUs: {
      In: [{ Title: "weibo", Link: "https://weibo.com/mayun" }],
      Out: [
        {
          Title: "twitter",
          Link: "https://twitter.com/jackma",
        },
      ],
    },
    Content: [],
    Honor: [
      {
        Name: "亚洲首富",
      },
      {
        Name: "马云公益基金会创始人",
      },
      {
        Name: "阿里巴巴集团创始人",
      },
      {
        Name: "2016年5月13日，获授法国荣誉军团勋章",
      },
      {
        Name: "世界经济论坛评选为中国的“未来全球领袖”",
      },
    ],
    Encyclopedia: [
      {
        Title: "维基百科",
        Link: "https://zh.wikipedia.org/wiki/%E9%A9%AC%E4%BA%91",
      },
      {
        Title: "百度百科",
        Link: "https://baike.baidu.com/item/%E9%A9%AC%E4%BA%91/6252",
      },
    ],
    TypeId: "enterprise",
    BaseUrl: "/p/mayun",
    Uid: "mayun",
  },
  // {
  //   LogoUrl: "",
  //   Title: "",
  //   Email: "",
  //   Location: "中国 ",
  //   Profile: "",
  //   Tags: [
  //     {
  //       Name: "",
  //     },
  //   ],
  //   FocusUs: {
  //     In: [
  //       {
  //         Title: "weibo",
  //         Link: "",
  //       },
  //       {
  //         Title: "bilibili",
  //         Link: "",
  //       },
  //       {
  //         Title: "toutiao",
  //         Link: "",
  //       },
  //     ],
  //     Out: [],
  //   },
  //   Content: [],
  //   Honor: [],
  //   BaseUrl: "/p/",
  //   Uid: "",
  // },
];
