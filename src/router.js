import Vue from "vue";
import VueRouter from "vue-router";
import index from "./views/index";
import blog from "./views/blog";
import explore from "./views/explore";
import personIndex from "./views/personIndex";
import typeIndex from "./views/typeIndex";
import blogIndex from "./views/blogIndex";
import notFound from "./views/notFound";

Vue.use(VueRouter);

const routerList = [
  {
    path: "/",
    name: "index",
    component: index,
    meta: {
      title: "打造属于个人的互联网名片 - BestHub",
    },
  },
  {
    path: "/blog",
    name: "blog",
    component: blog,
    meta: {
      title: "博客 - BestHub",
    },
  },
  {
    path: "/blog/:id/",
    name: "blogIndex",
    component: blogIndex,
    meta: {
      title: "BestHub",
    },
  },
  {
    path: "/p/:id",
    name: "personIndex",
    component: personIndex,
    meta: {
      title: "BestHub",
    },
  },
  {
    path: "/v/:id",
    name: "typeIndex",
    component: typeIndex,
    meta: {
      title: "BestHub",
    },
  },
  {
    path: "/explore",
    name: "explore",
    component: explore,
    meta: {
      title: "发现 - BestHub",
    },
  },
  {
    path: "/404",
    name: "404",
    component: notFound,
    meta: {
      title: "页面未找到 - BestHub",
    },
  },
  {
    path: "*",
    redirect: "/404",
  },
];

const router = new VueRouter({
  mode: "history",
  routes: routerList,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
